#!/usr/bin/env python

import os
import sys

# Read from environment, and set defaults for this demo project
FILE_PATH = os.environ.get('FILE_PATH', 'test.py')
PATTERN = os.environ.get('PATTERN', 'CI_VARIABLE')

fh = open(FILE_PATH, 'r')

match = False

while True:
    line = fh.readline()

    print(line)

    # This does a match with "pattern in line"
    # If the variable definition is required, change pattern to `CI_VARIABLE =` or similar
    if PATTERN in line:
        print("Found pattern {p} in line {l}".format(p=PATTERN, l=line))
        match = True

    # EOF
    if not line:
        break

fh.close()

if match:
    print("OK - Found Pattern '{p}' in file '{f}'".format(p=PATTERN,f=FILE_PATH))
    sys.exit(0)
else:
    print("ERROR: Pattern '{p}' not found in file '{f}'".format(p=PATTERN,f=FILE_PATH))
    sys.exit(1) # signal error to CI/CD pipeline
